function deepCopy(obj) {
    let tmp = JSON.stringify(obj)
    let result = JSON.parse(tmp)
    return result
}

let O = { name: '123', age: 12 }

let B = deepCopy(O);

O.name = '321';

// console.log(O, B);


for (var i = 0; i < 3; i++) {
    setTimeout(() => { console.log("var" + i) }, 1)
}


for (let i = 0; i < 3; i++) {
    setTimeout(() => { console.log("let" + i) }, 1)
}